var indexSectionsWithContent =
{
  0: "acdeghiklmnprstvw~",
  1: "cdgpr",
  2: "dgmprt",
  3: "cdeghimprstw~",
  4: "cdegiklmnprtw",
  5: "dev",
  6: "adp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Macros"
};

