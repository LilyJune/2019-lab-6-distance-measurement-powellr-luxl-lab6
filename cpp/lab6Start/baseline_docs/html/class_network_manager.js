var class_network_manager =
[
    [ "NetworkManager", "class_network_manager.html#ad9e6d52902d95a73c32eacacd8a44cc8", null ],
    [ "~NetworkManager", "class_network_manager.html#a2cfe4223139cf58587a9f066b956cb23", null ],
    [ "run", "class_network_manager.html#ab6e5674b07750e3368a3edf1b9c730e8", null ],
    [ "stop", "class_network_manager.html#afebd420c04cb8d9c23e4799c69dc0a36", null ],
    [ "portNumber", "class_network_manager.html#ac5fdb5010e4545c01579e36610935d85", null ],
    [ "referencequeue", "class_network_manager.html#a2b1286a17bc62339bb0cabcb3227e95d", null ],
    [ "server_fd", "class_network_manager.html#a689f4261b2748f946288dfae3c9fc75f", null ]
];