var class_motor_controller =
[
    [ "MotorController", "class_motor_controller.html#a124a6d57874f4584691917742c8a12fc", null ],
    [ "~MotorController", "class_motor_controller.html#ae4a6055b64466936c3576fb52fda9307", null ],
    [ "setDirection", "class_motor_controller.html#afe0c90360e0ff8043b8d20cc439dbe0d", null ],
    [ "setSpeed", "class_motor_controller.html#ad6537e115b5e5e671339b13181bfc991", null ],
    [ "stop", "class_motor_controller.html#a317ce58ea93ea1329de5b87391560082", null ],
    [ "backwardPin", "class_motor_controller.html#a217672e3ca63a3c4b93fa68faacd8e01", null ],
    [ "enablePin", "class_motor_controller.html#aab205779db32cc7e431043aa532b661e", null ],
    [ "forwardIOPin", "class_motor_controller.html#ad17b7346f045d6aebc40ac0d09fe10a4", null ],
    [ "managerInstance", "class_motor_controller.html#ab891f46aa877c6764c66d473f2796dab", null ],
    [ "speed", "class_motor_controller.html#a1d04910ab52d613a2e75c0d9f2b07fef", null ]
];