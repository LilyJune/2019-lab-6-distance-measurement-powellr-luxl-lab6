/**
 * @file main.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE This is a piece of a template solution to the lab.
 *
 *
 * @section DESCRIPTION
 *      This is the main method for the basic robot.  It will control the simplest operation of the robot over the network.
 */
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include "GPIO.h"
#include "RobotCfg.h"
#include "TaskRates.h"
#include "DistanceSensor.h"
#include "TaskRates.h"
#include "main.h"

using namespace std;
/**
 * This method will instantiate a new instance of the console printer.
 * @param ds This is a pointer to the distance sensor.
 * @param threadName This is the name of the thread that is to periodically be invoked.
 * @param period This is the period for the task.  This period is given in microseconds.
 */
ConsolePrinter::ConsolePrinter(se3910RPiHCSR04::DistanceSensor* ds, std::string threadName,	uint32_t period)
: PeriodicTask(threadName, period) {
	this->ds = ds;
}

/**
 * This is the task method.  The task method will be invoked periodically every taskPeriod
 * units of time.
 */
void ConsolePrinter::taskMethod()
{
	cout << "Current: " << ds->getCurrentDistance() << "\tAverage: " << ds->getAverageDistance()  << "\tMax: " << ds->getMaxDistance() << "\tMin: " << ds->getMinDistance() << "\n";
}

/**
 * This is the main program.  It will instantiate a network manager and robot controller as well as a Command Queue.
 * It will then block until the user enters a message on the console.
 * This will then cause it to shutdown the other classes and wait for their threads to terminate.
 */
int main(int argc, char* argv[]) {
	se3910RPiHCSR04::DistanceSensor *ds = new se3910RPiHCSR04::DistanceSensor(22, 27, "Distance Sensor", DISTANCE_SENSOR_TASK_RATE);
	ds->start(54);
//	ds->start();
	ConsolePrinter * cp = new ConsolePrinter(ds, "Console Thread", 250000);
	cp->start();

	char msg[1024];
	do {
		cin >> msg;
		cout << msg;

		if (msg[0]=='P')
		{
			ds->printThreads();
		}
		else if (msg[0]=='R')
		{
			ds->resetDistanceRanges();
		}
	}while (msg[0]!='Q');

	ds->stop();
	cp->stop();

	ds->waitForShutdown();
	cp->waitForShutdown();
	delete ds;
	delete cp;

	// Wait for the threads to die.
}






