/*
 * RobotController.cpp
 *
 *  Created on: Oct 9, 2019
 *      Author: Lily Lux
 */

#include "RobotController.h"
#include "NetworkCommands.h"

/**
 * Controls the directions of the robot.
 * returns command used.
 */
int RobotController::processMotionControlCommand(int command) {
	printf("Command %d\n", command);
    if (command == LEFT) {
        this->rightMotor->setDirection(1);
        this->leftMotor->setDirection(-1);
    } else if (command == RIGHT) {
        this->rightMotor->setDirection(-1);
        this->leftMotor->setDirection(1);
    } else if (command == STOP) {
        this->rightMotor->setDirection(0);
        this->leftMotor->setDirection(0);
    } else if (command == FORWARD) {
        this->rightMotor->setDirection(1);
        this->leftMotor->setDirection(1);
    } else if (command == BACKWARD) {
        this->rightMotor->setDirection(-1);
        this->leftMotor->setDirection(-1);
    } else if (command == (RIGHT + BACKWARD)) {
        //do move back right direction
        this->rightMotor->setDirection(0);
        this->leftMotor->setDirection(-1);
    } else if (command == (LEFT + BACKWARD)) {
        //do move back left direction
        this->rightMotor->setDirection(-1);
        this->leftMotor->setDirection(0);
    } else if (command == (RIGHT + FORWARD)) {
        //do move forward right
        this->rightMotor->setDirection(0);
        this->leftMotor->setDirection(1);
    } else if (command == (LEFT + FORWARD)) {
        //do move forward left
        this->rightMotor->setDirection(1);
        this->leftMotor->setDirection(0);
    }
    return command;
}

/**
 * sets both motors to the speed of the command
 * returns command
 */
int RobotController::processSpeedControlCommand(int command) {
	printf("Seting Speed to: %d\n", command);
	this->currentSpeed = command;
    this->rightMotor->setSpeed(command);
    this->leftMotor->setSpeed(command);
    return command;
}

/**
 * Constructor
 */
RobotController::RobotController(CommandQueue* queue, PWMManager& manager, std::string threadName): RunnableClass(threadName) {
    this->currentSpeed = 0;
    this->currentOperation = 0;
    this->referencequeue = queue;
    this->rightMotor = new MotorController(12,13,6,manager);
    this->leftMotor = new MotorController(20,21,26,manager);
}

/**
 * Destructor
 */
RobotController::~RobotController() {
    delete this->rightMotor;
    delete this->leftMotor;
}

/**
* This runs the robot. While keepGoing is true it loops.
* If the command greater than MOTORDIRECTIONBITMAP subtract that value and send the direction command.
* If the command is SPEEDDIRECTIONBITMAP then subtract that value and send the speed command.
*
*/
void RobotController::run() {
    while (this->keepGoing == true) {
        int com = this->referencequeue->dequeue();
        if (com > SPEEDDIRECTIONBITMAP) {
            int real = com - SPEEDDIRECTIONBITMAP;
            this->processSpeedControlCommand(real);
        } else if (com > MOTORDIRECTIONBITMAP) {
        	int real = com - MOTORDIRECTIONBITMAP;
            this->processMotionControlCommand(real);
        }
    }
}


/**
 * Stops superclass and the motors. Sends the STOP command to the queue.
 */
void RobotController::stop() {
    RunnableClass::stop();
    this->rightMotor->stop();
    this->leftMotor->stop();
    this->referencequeue->enqueue(MOTORDIRECTIONBITMAP + STOP);
}
