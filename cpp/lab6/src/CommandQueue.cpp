/*
 * CommandQueue.cpp
 *
 *  Created on: Oct 2, 2019
 *      Author: se3910
 */

#include "CommandQueue.h"


CommandQueue::CommandQueue() {
	sem_init(&queueCountSemaphore, 1, 0);
	commandQueueContents = std::queue<int>();
}

CommandQueue::~CommandQueue() {
	// TODO Auto-generated destructor stub
}

bool CommandQueue::hasItem() {
	queueMutex.lock();
	bool hasElement = !(commandQueueContents.empty());
    queueMutex.unlock();
	return hasElement;
}

int CommandQueue::dequeue() {
	sem_wait(&queueCountSemaphore); // blocks until above zero then decrements
	queueMutex.lock(); // race condition between decrementing and locking mutex may need to change.
	int element = commandQueueContents.front();
	commandQueueContents.pop();
	queueMutex.unlock();
    return element;
}

void CommandQueue::enqueue(int value) {
	queueMutex.lock();
	commandQueueContents.push(value);
	sem_post(&queueCountSemaphore);
	queueMutex.unlock();
}
