/**
 * @file DiagnosticManager.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file holds the interface for the diagnostic manager.  The diagnostic manager is responsible for reporting back robot diagnostics status to the controller.
 */

#ifndef DIAGNOSTICMANAGER_H_
#define DIAGNOSTICMANAGER_H_

#include "PeriodicTask.h"
#include "NetworkManager.h"
#include "DistanceSensor.h"

class DiagnosticManager: public PeriodicTask {
private:
	/**
	 * This is a pointer to the queue that will be used for receiving commands.  Commands can come from the network or other portions of the robot.
	 */
	CommandQueue* referencequeue;

	/**
	 * This is a pointer to the instance of the class which is to be used to transmit messages back to the controlling software.
	 */
	NetworkTransmissionManager* transmitInstance;

	/**
	 * This is the distance sensor instance that is to be used to determine the clearance in front of the robot.
	 */
	se3910RPiHCSR04::DistanceSensor* dsInstance;

public:
	/**
	 * This is the constructor for the class.
	 * @param queue This is a pointer to the queue that is to be used to read commands destined for this module.
	 * @param transmitInstance This si the instance of the network transmission class, which is where all messages will be sent from.
	 * @param ds This is a pointer tio the instance of the distance sensor that is to be used by this class.
	 * @param threadName This is the name of the periodic task thread.
	 * @param period This is the period for the task, given in microseconds.
	 */
	DiagnosticManager(CommandQueue *queue, NetworkTransmissionManager* transmitInstance, se3910RPiHCSR04::DistanceSensor *ds, std::string threadName,
			uint32_t period);
	/**
	 * This is the destructor which will clean up after the class.
	 */
	virtual ~DiagnosticManager();

	/**
	 * This is the task method.  The task method will be invoked periodically every taskPeriod
	 * units of time.
	 */
	virtual void taskMethod();
};

#endif /* DIAGNOSTICMANAGER_H_ */
