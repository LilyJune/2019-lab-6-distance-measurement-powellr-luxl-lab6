/**
 * @file NetworkManager.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file defines the implementation for the Network Manager.  The Network Manager manages network connections and acts as a server, receiving messages sent over a socket.
 */

#include "NetworkManager.h"

#include "NetworkCfg.h"
#include "CommandQueue.h"
#include "NetworkMessage.h"
#include "NetworkCommands.h"
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <string>

using namespace std;

/**
 * This is the constructor for the Network Manager.  It will instantiate a new instance of the class.
 * @param port This is the port that the network manager is to listen on for incoming connections.
 * @param queue This is the queue that is to be used for enqueueing received requests.
 * @param threadName This is the name given to the executing thread.  It is a simple, string that is human readable.
 */
NetworkReceptionManager::NetworkReceptionManager(int port, CommandQueue **queue, std::string threadName) : RunnableClass(threadName) {
	portNumber = port;
	myThread = NULL;
	keepGoing = true;
	referencequeue = queue;
}

/**
 * This is the destructor for the class which will clean up from the instantiation and operation of the class.
 */
NetworkReceptionManager::~NetworkReceptionManager() {
	/**
	 * Right now, this destructor doesn't have anything to do, as there are no dynamically allocated objects that need to be destroyed.
	 */
}

/**
 * This method will override the default stop method.  In doing so, it must call the base class's stop method prior to invoking it's own logic.
 */
void NetworkReceptionManager::stop() {
	/**
	 * Call the parent's stop method.
	 */
	RunnableClass::stop();

	/**
	 * Call shutdown on the socket to shut down the full-duplex connection.
	 */
	shutdown(server_fd, SHUT_RDWR);
}

/**
 * This method will return the socket ID for the given class.
 * @return The socket ID will be returned.
 */
int NetworkReceptionManager::getSocketID() {
	return socketID;
}

/**
 * This is the run method for the class.  It contains the code that is to run periodically on the given thread.
 */
void NetworkReceptionManager::run() {
	int valread;
	struct sockaddr_in serverAddress;
	struct sockaddr_in clientAddress;
	int opt = 1;
	int addrlen = sizeof(clientAddress);
	int socketOpen;
	networkMessageStruct receivedMessage;

	/**
	 * Create a socket file descriptor.  The socket is a tcp socket.
	 */
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		return;
	}

	/**
	 * Call setsockopt to setup the options on the socket.
	 *
	 */
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt,
			sizeof(opt))) {
		perror("setsockopt");
		return;
	}
	/**
	 * The connection is to be using an internet based myAddress format.
	 */
	serverAddress.sin_family = AF_INET;

	/**
	 * The connection can be made from any address.
	 */
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	/**
	 * Setup which port to listen on.
	 */
	serverAddress.sin_port = htons(portNumber);

	/**
	 * Now bind the listening socket to the correct port.
	 */
	if (bind(server_fd, (struct sockaddr *) &serverAddress,
			sizeof(serverAddress)) < 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	/**
	 * Loop so long as the thread is to continue running.
	 */
	while (keepGoing) {
		/**
		 * Listen for and accept the given connection.
		 */
		if (listen(server_fd, 3) < 0) {
			perror("listen");
			exit(EXIT_FAILURE);
		}
		if (((socketID = accept(server_fd, (struct sockaddr *) &clientAddress,
				(socklen_t*) &addrlen)) < 0) && (keepGoing)) {
			return;
		}
		socketOpen = sizeof(networkMessageStruct);

		/**
		 * So long as the socket remains open.
		 */
		while ((socketOpen > 0) && (keepGoing)) {
			valread = 0;
			char* buf = (char*) &receivedMessage;

			/**
			 * Receive the message.  Make sure to wait on all of the message being received.
			 */
			valread = recv(socketID, buf, sizeof(networkMessageStruct),
			MSG_WAITALL);

			if (valread == 0) {
				/**
				 * If we receive 0 bytes, the socket has been closed so abort.
				 */
				socketOpen = 0;
				close(socketID);
			} else {
				/**
				 * Convert the message to the appropriate endian format.
				 * Do this by converting each individual structure element accordingly.
				 */
				receivedMessage.message = ntohl(receivedMessage.message);
				receivedMessage.messageDestination = ntohl(
						receivedMessage.messageDestination);
				receivedMessage.xorChecksum = ntohl(
						receivedMessage.xorChecksum);

				/**
				 * Now check that the message was properly received by verifying the checksum.
				 **/
				if ((receivedMessage.message
						^ receivedMessage.messageDestination)
						== receivedMessage.xorChecksum) {
					/**
					 * The message is valid.  Enqueue it to the right queue if the destination is valid.
					 */
					if ((receivedMessage.messageDestination > 0)
							&& (receivedMessage.messageDestination
									<= NUMBER_OF_QUEUES)) {
						(*(referencequeue[receivedMessage.messageDestination - 1])).enqueue(
								receivedMessage.message);
					}
				}
			}
		}
	}
}

NetworkTransmissionManager::NetworkTransmissionManager(NetworkReceptionManager* associatedReceptionManager, std::string threadName) : RunnableClass(threadName)
{
	this->associatedReceptionManager=associatedReceptionManager;
	/**
	 * Initialize the semaphore to be a counting sem with nothing on it.
	 **/
	sem_init(&queueCountSemaphore, 0, 0);

}

NetworkTransmissionManager::~NetworkTransmissionManager() {
	/**
	 * Nothing is needed here for now, as there is nothing dynamically created that we need to clean up afterwards.
	 */
}

void NetworkTransmissionManager::enqueueMessage(
		networkMessageStruct &itemToEnqueue) {
	// Lock the queue.
	std::lock_guard<std::mutex> guard(queueMutex);
	// Place the given item on the end of the queue.
	transmissionQueue.push(itemToEnqueue);
	// Indicate that something has been enqueued through the semaphore.
	sem_post(&queueCountSemaphore);
}
/**
 * This is the virtual run method.  It will execute the given code that is to be executed by this class.
 */
void NetworkTransmissionManager::run() {
	while (keepGoing) {
		networkMessageStruct itemToTransmit;

		/**
		 * Block if there is nothing on the queue until something is enqueued.
		 */
		sem_wait(&queueCountSemaphore);
		{
			/**
			 * Lock the queue and dequeue an item from it.
			 */
			std::lock_guard<std::mutex> guard(queueMutex);

			/**
			 * Obtain the first item from the queue and then dispose of it.
			 */
			itemToTransmit = transmissionQueue.front();
			transmissionQueue.pop();
		}

		// Now that we have an item to transmit,
		itemToTransmit.message = htonl(itemToTransmit.message);
		itemToTransmit.messageDestination = htonl(
				itemToTransmit.messageDestination);
		itemToTransmit.xorChecksum = htonl(itemToTransmit.xorChecksum);

		if (associatedReceptionManager->getSocketID() > 0) {

			// Now send it.
			send(associatedReceptionManager->getSocketID(), &itemToTransmit,
					sizeof(networkMessageStruct), 0);
		}

	}
}
